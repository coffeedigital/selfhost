﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.Domain.Projects
{
    public interface IProjectRepository
    {
        Project GetByKey(int Id);
        List<Project> GetAll();
        void Add(Project project);
        void Update(Project project);
        void Remove(Project project);

    }
}
