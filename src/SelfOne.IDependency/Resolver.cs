﻿using Self.Infra.Data;
using SelfOne.Business.Services.Projects;
using SelfOne.Domain.Data;
using SelfOne.Domain.Projects;
using SelfOne.Infra;
using SelfOne.Repository.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace SelfOne.IDependency
{
    public static class Resolver
    {
        public static void Resolve(UnityContainer container)
        {
            container.RegisterType<IProjectRepository, ProjectRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IProjectService, ProjectService>(new HierarchicalLifetimeManager());
            container.RegisterType<IDataConnection, DataConnection>(new HierarchicalLifetimeManager());
            container.RegisterType<AppDataContext, AppDataContext>();
        }

    }
}
