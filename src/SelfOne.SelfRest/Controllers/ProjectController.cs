﻿using SelfOne.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SelfOne.SelfRest.Controllers
{
    [RoutePrefix("api/project")]
    public class ProjectController : ApiController
    {
        private IProjectRepository _projectRepository;
        private IProjectService _projectService;

        public ProjectController(
            IProjectRepository projectRepository,
            IProjectService projectService)
        {
            _projectRepository = projectRepository;
            _projectService = projectService;
        }

        [HttpGet]
        public Task<HttpResponseMessage> GetAll()
        {

            var response = new HttpResponseMessage();

            try
            {
                var listProject = new List<Project>();
                listProject.Add(new Project()
                {
                    Active = "Y",
                    Description = "Projeto teste",
                    Id = 1,
                    PrjCode = "PJR002",
                    PrjName = "Nome do projeto"
                });
                //var listProjects = _projectRepository.GetAll();
                response = Request.CreateResponse(HttpStatusCode.OK, listProject);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            var TaskResult = new TaskCompletionSource<HttpResponseMessage>();
            TaskResult.SetResult(response);
            return TaskResult.Task;

        }

        [Route("{Id}")]
        [HttpGet]
        public Task<HttpResponseMessage> GetByKey(int Id)
        {
            var response = new HttpResponseMessage();
            try
            {
                var projectFounded = _projectRepository.GetByKey(Id);
                response = Request.CreateResponse(HttpStatusCode.OK, projectFounded);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            var TaskResult = new TaskCompletionSource<HttpResponseMessage>();
            TaskResult.SetResult(response);
            return TaskResult.Task;
        }


        [HttpPost]
        public Task<HttpResponseMessage> Add(Project project)
        {
            var response = new HttpResponseMessage();
            try
            {
                _projectService.Add(project);
                response = Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            var TaskResult = new TaskCompletionSource<HttpResponseMessage>();
            TaskResult.SetResult(response);
            return TaskResult.Task;
        }

        [HttpPut]
        public Task<HttpResponseMessage> Update(Project project)
        {
            var response = new HttpResponseMessage();
            try
            {
                _projectService.Update(project);
                response = Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            var TaskResult = new TaskCompletionSource<HttpResponseMessage>();
            TaskResult.SetResult(response);
            return TaskResult.Task;
        }

        [HttpDelete]
        public Task<HttpResponseMessage> Remove(Project project)
        {
            var response = new HttpResponseMessage();
            try
            {
                _projectService.Remove(project);
                response = Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            var TaskResult = new TaskCompletionSource<HttpResponseMessage>();
            TaskResult.SetResult(response);
            return TaskResult.Task;
        }

    }
}
