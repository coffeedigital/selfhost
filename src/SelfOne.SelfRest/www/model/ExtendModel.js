jQuery.sap.require("sap.ui.model.json.JSONModel");

sap.ui.model.json.JSONModel.extend("One.model.ExtendModel", {
	
	loadDataNew: function(sURL, fnSuccess, fnError, oParameters, bAsync, sType, bMerge, bCache, dataType, contentType){
			
			var that = this;

			if (bAsync !== false) {
				bAsync = true;
			}
			if (!sType)	{
				sType = "GET";
			}
			if (bCache === undefined) {
				bCache = this.bCache;
			}
			
			this.fireRequestSent({url : sURL, type : sType, async : bAsync, info : "cache="+bCache+";bMerge=" + bMerge});

			jQuery.ajax({
			  url: sURL,
			  async: bAsync,
			  dataType: dataType,
			  contentType : contentType,
			  cache: bCache,
			  data: oParameters,
			  type: sType,
			  success: function(oData) {
				if (!oData) {
					jQuery.sap.log.fatal("The following problem occurred: No data was retrieved by service: " + sURL);
				}
				that.oDataOrig = {};
				that.oDataOrig = jQuery.extend(true,{},that.oDataOrig, oData); 
				that.setData(oData, bMerge);
				that.fireRequestCompleted({url : sURL, type : sType, async : bAsync, info : "cache=false;bMerge=" + bMerge});
				if (typeof fnSuccess === 'function') {
                    fnSuccess(oData);
                }

			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown){
				jQuery.sap.log.fatal("The following problem occurred: " + textStatus, XMLHttpRequest.responseText + ","
							+ XMLHttpRequest.status + "," + XMLHttpRequest.statusText);
				that.fireRequestCompleted({url : sURL, type : sType, async : bAsync, info : "cache=false;bMerge=" + bMerge});
				that.fireRequestFailed({message : textStatus,
					statusCode : XMLHttpRequest.status, statusText : XMLHttpRequest.statusText, responseText : XMLHttpRequest.responseText});
				if (typeof fnError === 'function') {
                    fnError({message : textStatus, statusCode : XMLHttpRequest.status, statusText : XMLHttpRequest.statusText, responseText : XMLHttpRequest.responseText});
                }
			  }
			});

	},
	
	getOrigData: function(){
		return this.oDataOrig; 
	},
	
	discardChanges: function(){
		this.setData(this.oDataOrig); 
	},
	
	commitChanges: function(){
		this.oDataOrig = this.getData();		
    },    

    insert:function(url, fnSuccess,fnerror){
       this.loadDataNew(url,fnSuccess, fnerror, JSON.stringify(this.getData()), true, 'POST', false, false, "json", "application/json");
    },
    updateList:function(url, fnSuccess,fnerror){
		this.loadDataNew(url,fnSuccess, fnerror, JSON.stringify(this.getData()), true,'POST', false, false, null, "application/json")
	},
    update:function(url, fnSuccess, fnerror, returType="application/json"){
        this.loadDataNew(url, fnSuccess , fnerror, this.getData(), true, 'PUT', false, false, returType );        

    },
    load:function(url, fnSuccess, fnerror){
        this.loadDataNew(url, fnSuccess , fnerror, null, true, 'GET', false, false, "json");        
    },  
    remove:function(url, fnSuccess, fnerror){
        this.loadDataNew(url, fnSuccess , fnerror, this.getData(), true, 'DELETE', false, false, "json");        
	},
	
	loadPost:function(url, oParameters, fnSuccess, fnerror){
        this.loadDataNew(url, fnSuccess , fnerror, oParameters, true, 'POST', false, false, "json", "application/json");        
	},
	
	sendPostToApi(url, fnsuccess, fnError){
		this.insert(url, this.getData(), fnsuccess, fnError);
	},
	
	updateByUrl:function(url, fnSuccess, fnerror){
        this.loadDataNew(url, fnSuccess, fnerror, null, true, 'PUT');        
	},

});