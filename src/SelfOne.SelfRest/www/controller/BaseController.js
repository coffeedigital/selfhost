let Libs = [
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"One/model/formatter",
	"sap/ui/model/json/JSONModel",	
	"sap/m/MessageToast",	
	'sap/ui/Device',
	'sap/m/ActionSheet',
	'sap/m/Button',
	'sap/m/ResponsivePopover',
	'sap/m/NotificationListItem',
	'sap/m/MessagePopover',
	'sap/m/Link',
	'sap/ui/core/CustomData',
	'sap/m/MessagePopoverItem'
];


sap.ui.define(Libs, function (Controller, History, formatter, JSONModel, MessageToast, Device, ActionSheet, Button, ResponsivePopover, NotificationListItem, MessagePopover, Link, CustomData, MessagePopoverItem) {

	return Controller.extend("One.controller.BaseController", {		
		
		onInit:function(){

		},
		
		onSideNavButtonPress: function() {
			var user = this.getUserSession();
			if(!user){
				var oToolPage = this.byId("app");
				var bSideExpanded = oToolPage.getSideExpanded();
				oToolPage.setSideExpanded(!oToolPage.getSideExpanded());
			}else{
				MessageToast.show(this.getText("Auth.NotConected"));
			}
		},

		_setToggleButtonTooltip : function(bSideExpanded) {
			var oToggleButton = this.byId('sideNavigationToggleButton');
			if (bSideExpanded) {
				oToggleButton.setTooltip('Large Size Navigation');
			} else {
				oToggleButton.setTooltip('Small Size Navigation');
			}
		},



		onUserNamePress: function(oEvent) {
			var oBundle = this.getModel("i18n").getResourceBundle();
			var oMessagePopover = this.byId("errorMessagePopover");
			if (oMessagePopover && oMessagePopover.isOpen()) {
				oMessagePopover.destroy();
			}
			var fnHandleUserMenuItemPress = function (oEvent) {
				MessageToast.show(oEvent.getSource().getText() + " was pressed");
			};
			var oActionSheet = new ActionSheet(this.getView().createId("userMessageActionSheet"), {
				title: oBundle.getText("userHeaderTitle"),
				showCancelButton: false,
				buttons: [
					new Button({
						text: 'User Settings',
						type: sap.m.ButtonType.Transparent,
						press: fnHandleUserMenuItemPress
					}),
					new Button({
						text: "Online Guide",
						type: sap.m.ButtonType.Transparent,
						press: fnHandleUserMenuItemPress
					}),
					new Button({
						text: 'Feedback',
						type: sap.m.ButtonType.Transparent,
						press: fnHandleUserMenuItemPress
					}),
					new Button({
						text: 'Help',
						type: sap.m.ButtonType.Transparent,
						press: fnHandleUserMenuItemPress
					}),
					new Button({
						text: 'Logout',
						type: sap.m.ButtonType.Transparent,
						press: fnHandleUserMenuItemPress
					})
				],
				afterClose: function () {
					oActionSheet.destroy();
				}
			});
			jQuery.sap.syncStyleClass(this.getView().getController().getOwnerComponent().getContentDensityClass(), this.getView(), oActionSheet);
			oActionSheet.openBy(oEvent.getSource());
		},




			onMessagePopoverPress: function (oEvent) {
				if (!this.byId("errorMessagePopover")) {
					var oMessagePopover = new MessagePopover(this.getView().createId("errorMessagePopover"), {
						placement: sap.m.VerticalPlacementType.Bottom,
						items: {
							path: 'alerts>/alerts/errors',
							factory: this._createError
						},
						afterClose: function () {
							oMessagePopover.destroy();
						}
					});
					this.byId("app").addDependent(oMessagePopover);
					jQuery.sap.syncStyleClass(this.getView().getController().getOwnerComponent().getContentDensityClass(), this.getView(), oMessagePopover);
					oMessagePopover.openBy(oEvent.getSource());
				}
			},



			onNotificationPress: function (oEvent) {
				var oBundle = this.getModel("i18n").getResourceBundle();
				var oMessagePopover = this.byId("errorMessagePopover");
				if (oMessagePopover && oMessagePopover.isOpen()) {
					oMessagePopover.destroy();
				}
				var oButton = new Button({
					text: oBundle.getText("notificationButtonText"),
					press: function () {
						MessageToast.show("Show all Notifications was pressed");
					}
				});
				var oNotificationPopover = new ResponsivePopover(this.getView().createId("notificationMessagePopover"), {
					title: oBundle.getText("notificationTitle"),
					contentWidth: "300px",
					endButton : oButton,
					placement: sap.m.PlacementType.Bottom,
					content: {
						path: 'alerts>/alerts/notifications',
						factory: this._createNotification
					},
					afterClose: function () {
						oNotificationPopover.destroy();
					}
				});
				this.byId("app").addDependent(oNotificationPopover);
				jQuery.sap.syncStyleClass(this.getView().getController().getOwnerComponent().getContentDensityClass(), this.getView(), oNotificationPopover);
				oNotificationPopover.openBy(oEvent.getSource());
			},



			_createNotification: function (sId, oBindingContext) {
				var oBindingObject = oBindingContext.getObject();
				var oNotificationItem = new NotificationListItem({
					title: oBindingObject.title,
					description: oBindingObject.description,
					priority: oBindingObject.priority,
					close: function (oEvent) {
						var sBindingPath = oEvent.getSource().getCustomData()[0].getValue();
						var sIndex = sBindingPath.split("/").pop();
						var aItems = oEvent.getSource().getModel("alerts").getProperty("/alerts/notifications");
						aItems.splice(sIndex, 1);
						oEvent.getSource().getModel("alerts").setProperty("/alerts/notifications", aItems);
						oEvent.getSource().getModel("alerts").updateBindings("/alerts/notifications");
						sap.m.MessageToast.show("Notification has been deleted.");
					},
					datetime: oBindingObject.date,
					authorPicture: oBindingObject.icon,
					press: function () {
						var oBundle = this.getModel("i18n").getResourceBundle();
						MessageToast.show(oBundle.getText("notificationItemClickedMessage", oBindingObject.title));
					},
					customData : [
						new CustomData({
							key : "path",
							value : oBindingContext.getPath()
						})
					]
				});
				return oNotificationItem;
			},


			onItemSelect: function(oEvent) {
				var user = this.getUserSession();

				var oItem = oEvent.getParameter('item');
				var sKey = oItem.getKey();

				if (!user) {
					if (Device.system.phone) {
						this.onSideNavButtonPress();
					}
					this.getRouter().navTo(sKey);
				} else {
					MessageToast.show(this.getText("Auth.NotConected"));
				}
			},



			_createError: function (sId, oBindingContext) {
				var oBindingObject = oBindingContext.getObject();
				var oLink = new Link("moreDetailsLink", {
					text: "More Details",
					press: function() {
						MessageToast.show("More Details was pressed");
					}
				});
				var oMessageItem = new MessagePopoverItem({
					title: oBindingObject.title,
					subtitle: oBindingObject.subTitle,
					description: oBindingObject.description,
					counter: oBindingObject.counter,
					link: oLink
				});
				return oMessageItem;
			},




		getRouter : function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		setUserTheme : function(){
			var user = this.getUserSession();
			if(user.UserSettings == undefined) 
				return;
			if(!user.UserSettings.Theme)
				return;

			var theme = sap.ui.getCore().getConfiguration().getTheme();
			
			if(theme != user.UserSettings.Theme)
				sap.ui.getCore().applyTheme(user.UserSettings.Theme); 
		},

		getApiUrl(){
			let base = ["http://localhost:19127/api"];
			for (let index = 0; index < arguments.length; index++) {
				const element = arguments[index];
				base.push(element);
			}

			return base.join('/');
		},
		
		api : {							
			Page:"Page/Page"
		},

		getModel : function (sName) {
			return this.getView().getModel(sName);
		},

		formatRequestPurchaseState : function(state){
			return formatter.formatRequestPurchaseState(state);
		},	

		formatDimActive : function(dimActive){
			return formatter.dimActive(dimActive);
		},
		setUserModel : function(user){
			var userModel = new JSONModel();
            userModel.setData(user);
            sap.ui.getCore().setModel(userModel, "currentUser");			
		},
		setModel : function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getResourceBundle : function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		getText: function(sKey){
			return this.getResourceBundle().getText(sKey);
		},

		onNavBack : function(sRoute, mData) {
			 var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {		 		
				window.history.go(-1);				
			 } else {				
				this.redirectIfLogged();
			} 
		},	
		
		getIndexOfPath : function(sPath){
			var pathArray = sPath.split("/");
			var sIndex = pathArray[pathArray.length - 1];
			var index = Number.parseInt(sIndex);
			return index;
		},	

		getUserSession : function(){
            var user = JSON.parse(sessionStorage.getItem('currentUser'));
            if(!user || !user.Token)
                return false;
                
            return user;
		},
		setUserSession : function(sUser){
			var user = JSON.stringify(sUser);
			sessionStorage.setItem('currentUser', user)            
		},
		setUserSessionToken : function(UserAcessToken){
			var user = JSON.parse(sessionStorage.getItem('currentUser'));
			if(!user)
				return;
			user.Token = UserAcessToken;
			var user = JSON.stringify(user);
			sessionStorage.setItem('currentUser', user);
		},
		
		destroyUserSession : function(){
			sessionStorage.clear();
            sap.ui.getCore().setModel(new JSONModel(this.EmptyUser), "currentUser");			
			
		},
		
		redirectIfLogged : function(){
			var user = this.getUserSession();
			if(user)
				this.getRouter().navTo("dashBoard");
		},

		redirectIfNotLogged : function(){
			var user = this.getUserSession();
			if(!user)
				this.getRouter().navTo("auth");
		},

		loadModel(context, endPoint, nameModel, busyControls, refresh = false ){
			let model = new JSONModel();
			
			model.attachRequestSent(data => { busyControls.forEach(x => x.setBusy(true))});
            model.attachRequestCompleted(data =>{ busyControls.forEach(x => x.setBusy(false) )});
            model.attachRequestFailed(data =>{ busyControls.forEach(x => x.setBusy(false))});            
			model.loadData(endPoint);
			context.setModel(model, nameModel);
			if(refresh)
				context.getView().getModel(nameModel).refresh(true);
		},
		
		loadModelForDialog(context, endPoint, busyControls, successFunction, errorFunction ){
			let model =new JSONModel();
			
			model.attachRequestSent(data => {busyControls.forEach(x => x.setBusy(true))});
            model.attachRequestCompleted(data => { 
				busyControls.forEach(x => x.setBusy(false) );
				var requestReturn = data.getParameters();
				if(requestReturn.success){
					context.setModel(model);
					if(successFunction)
						successFunction();
				}else {
					MessageToast.show(requestReturn.errorobject.responseText);
					if(errorFunction)
						errorFunction();
						
				}
			});			
            model.attachRequestFailed(data =>{ busyControls.forEach(x => x.setBusy(false))});            
			model.loadData(endPoint);	
		},		
	});
});