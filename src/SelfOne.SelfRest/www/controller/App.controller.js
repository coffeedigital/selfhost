sap.ui.define([
		'One/controller/BaseController',
		'jquery.sap.global',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		'sap/m/ResponsivePopover',
		'sap/m/MessagePopover',
		'sap/m/ActionSheet',
		'sap/m/Button',
		'sap/m/Link',
		'sap/m/Bar',
		'sap/ui/layout/VerticalLayout',
		'sap/m/NotificationListItem',
		'sap/m/MessagePopoverItem',
		'sap/ui/core/CustomData',
		'sap/m/MessageToast',
		'sap/ui/Device',
	], function (BaseController,
		jQuery,
		Fragment,
		Controller,
		JSONModel,
		ResponsivePopover,
		MessagePopover,
		ActionSheet,
		Button,
		Link,
		Bar,
		VerticalLayout,
		NotificationListItem,
		MessagePopoverItem,
		CustomData,
		MessageToast,
		Device) {

		"use strict";

		return BaseController.extend("One.controller.App", {

			_bExpanded: true,

			onInit: function() {
				this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
				
				if (Device.resize.width <= 1024) {
					this.onSideNavButtonPress();
				}
				
				Device.media.attachHandler(function (oDevice) {
					if ((oDevice.name === "Tablet" && this._bExpanded) || oDevice.name === "Desktop") {
						this.onSideNavButtonPress();
						this._bExpanded = (oDevice.name === "Desktop");
					}
				}.bind(this));

				this.setSideSettings();
			},


			setSideSettings:function(){
				var oToolPage = this.byId("app");
				 oToolPage.setSideExpanded(!oToolPage.getSideExpanded());
			}

		});
	});

