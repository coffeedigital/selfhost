﻿using Microsoft.Owin.StaticFiles.ContentTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.SelfRest
{
    public class Provider : FileExtensionContentTypeProvider
    {
        public Provider()
        {
            Mappings.Add(".json", "application/json");
            Mappings.Add(".properties", "application/textplan");
        }
    }
}
