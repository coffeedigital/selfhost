﻿using SelfOne.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.Business.Services.Projects
{
    public class ProjectService : IProjectService
    {
        private IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public void Add(Project project)
        {
            project.Validate();
            _projectRepository.Add(project);
        }

        public List<Project> GetAll()
        {
            return _projectRepository.GetAll();
        }

        public Project GetByKey(int Id)
        {
            return _projectRepository.GetByKey(Id);
        }

        public void Remove(Project project)
        {
            var projectFounded = _projectRepository.GetByKey(project.Id);

            if (projectFounded == null)
                throw new Exception("Projeto não encontrado!");

            _projectRepository.Remove(project);
        }

        public void Update(Project project)
        {
            project.Validate();

            var projectFounded = _projectRepository.GetByKey(project.Id);
            if (projectFounded == null)
                throw new Exception("Projeto não encontrado!");

            _projectRepository.Update(project);
        }
    }
}
