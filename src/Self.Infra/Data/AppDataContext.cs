﻿using SelfOne.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.Infra
{
    public class AppDataContext: DbContext 
    {

        public AppDataContext()
    :           base("c1")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.UseDatabaseNullSemantics = false;
            this.Configuration.ValidateOnSaveEnabled = false;
        }

        public DbSet<Project> Project { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MapProject());
        }
    }
}
